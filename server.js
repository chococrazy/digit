var express = require('express');
var http    = require('http');

var path             = require('path');
var cookieParser     = require('cookie-parser');
var bodyParser       = require('body-parser');
var expressValidator = require('express-validator');
var jwt              = require('jsonwebtoken');
var multer         = require('multer');
var uploadIMG      = multer({ dest: 'uploads/images' })
// Models
var User = require('./models/User');

// Controllers
var userController     = require('./controllers/user');
var categoryController = require('./controllers/category');
var projectController = require('./controllers/project');

var app = express();



app.set('port',   3009);
app.use(bodyParser.json({limit: '500mb'}));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(expressValidator());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


app.use(function(req, res, next) {
  req.isAuthenticated = function() {
    var token = (req.headers.authorization && req.headers.authorization.split(' ')[1]) || req.cookies.token;
    try {
      return jwt.verify(token, 'a3e31f8e2ca0ce3554e49622109fe1798d9cddf33603fc4f9bcec17cea1ee5a5');
    } catch (err) {
      // console.log(err);
      return false;
    }
  };

  if (req.isAuthenticated()) {
    var payload = req.isAuthenticated();
    new User({ id: payload.sub })
      .fetch()
      .then(function(user) {
        if(!user)
        {
          next();
          return;
        }
        req.user = user;
        next();
      });
  } else {
    next();
  }
});


app.get('/category/list'       , categoryController.listCategory );
app.get('/category/single/:id' , categoryController.listSingleCategory );
app.post('/category/add'       , userController.ensureAuthenticated       , categoryController.addCategory );
app.post('/category/edit'      , userController.ensureAuthenticated       , categoryController.updateCategory );
app.post('/category/delete'    , userController.ensureAuthenticated       , categoryController.deleteCategory );
app.get('/api/project/filterCat'    , userController.ensureAuthenticated       , projectController.getTags );



app.get('/project/list'       , projectController.listProject );
app.get('/project/list/featured'       , projectController.listProjectFeatured );
app.get('/project/list/forTech'       , projectController.forTech );



app.get('/project/single/:id' , projectController.listSingleProject );
app.post('/project/add'       , userController.ensureAuthenticated       , projectController.addProject );
app.post('/project/edit'      , userController.ensureAuthenticated       , projectController.updateProject );
app.post('/project/delete'    , userController.ensureAuthenticated       , projectController.deleteProject );
app.post('/project/edit/file'      , userController.ensureAuthenticated ,  uploadIMG.single('file')    , projectController.updateProjectImage );
app.use("/downloads/images",express.static(path.join(__dirname, 'uploads/images')));


app.get('/me'            , userController.ensureAuthenticated , function(req, res, next){
  console.log()
  if(req.user)
  res.status(200).send({
    user : req.user.toJSON(),
    ok   : true
  });
  else {
    res.status(400).send({ok:false})
  }
});

app.get('/signup'       , userController.signupPost);
app.post('/login'        , userController.loginPost);




app.get('/admin_panel*', function(req, res) {
  // console.log(req.originalUrl)
  res.redirect('/admin_panel/#' + req.originalUrl.replace("/admin_panel/", ""));
});

app.get('/*', function(req, res) {
  res.redirect('/#' + req.originalUrl);
});

// Production error handler
if (app.get('env') === 'production') {
  app.use(function(err, req, res, next) {
    console.error(err.stack);
    res.sendStatus(err.status || 500);
  });
}



app.listen(3009, function() {
  console.log('Express server listening on port ' + app.get('port'));
});

module.exports = app;
