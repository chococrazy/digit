var animationEnd = (function(el) {
  var animations = {
    animation: 'animationend',
    OAnimation: 'oAnimationEnd',
    MozAnimation: 'mozAnimationEnd',
    WebkitAnimation: 'webkitAnimationEnd',
  };

  for (var t in animations) {
    if (el.style[t] !== undefined) {
      return animations[t];
    }
  }
})(document.createElement('div'));


angular.module('MyApp', ['ngRoute', 'satellizer'])
  .config(function($routeProvider, $locationProvider, $authProvider) {
    $locationProvider.html5Mode(true);




    $routeProvider
    .when('/', {
      //controller: 'HomeCtrl',
      templateUrl: 'partials/main.html'
    })
    .when('/pentagon', {
      //controller: 'HomeCtrl',
      templateUrl: 'partials/pentagon.html'
    })
    .when('/solution_catalogue', {
      //controller: 'HomeCtrl',
      templateUrl: 'partials/solution_catalogue.html'
    })


    .when('/options', {
      //controller: 'HomeCtrl',
      templateUrl: 'partials/options.html'
    })

    .when('/projectsMain', {
      controller: 'HomeCtrl',
      templateUrl: 'partials/home.html'
    })

    .when('/technology/:tech_name', {
      controller: 'HomeCtrl',
      templateUrl: 'partials/home.html'
    })

			.when('/technologies', {
        controller: 'UnitsCtrl',
        templateUrl: 'partials/units.html'
      })

      .when('/login', {
        templateUrl: 'partials/login.html',
        controller: 'LoginCtrl',
        resolve: { skipIfAuthenticated: skipIfAuthenticated }
      })


      .otherwise({
        templateUrl: 'partials/404.html'
      });

    $authProvider.loginUrl = '/login';
    $authProvider.signupUrl = '/signup';

    function skipIfAuthenticated($location, $auth) {
      if ($auth.isAuthenticated()) {
        $location.path('/');
      }
    }

  })
  .run(function($rootScope, $window, $http, $auth) {
    $rootScope.lang = 'en';



    $http.get('/settings/list/default').then(function(response){
        if(response.data.ok)
        {
          $rootScope.settings = $rootScope.formatSettings(response.data.settings);
          // console.log($rootScope.settings);
        }
    });

    if($window.localStorage.user)
    {
      $http.get('/me').then(function(res){
        // alert()
        if(res.data && res.data.user)
        {


          if(res.data.user.status  == false)
          {
            $auth.logout();
            delete $window.localStorage.user;
            $location.path('/');
          }

          // $rootScope.currentUser = response.data.user;
          // $window.localStorage. = ;
          localStorage.setItem('user', JSON.stringify(res.data.user));


          if(res.data.user.role != 'admin')
          {
            // window.location.href = window.location.origin + "/account"
          }

        }else{
          $auth.logout();
          delete $window.localStorage.user;
          $location.path('/');
        }

      })
      .catch(function(er){
        $auth.logout();
        delete $window.localStorage.user;
        window.location.href = window.location.origin + "/login"
      })
    }

    $rootScope.formatSettings = function(data){
      var set = {};
      for(var i = 0 ;i < data.length; i++ )
      {
        tmp  = data[i].key;
        set[tmp] = data[i].content;
        set[tmp+'_he'] = data[i].content_hebrew;
      }

      console.log(set)
      return set;

    }

    $rootScope.$on("$routeChangeSuccess", function (event, currentRoute, previousRoute) {
      setTimeout(function(){
        if($('#no_KYC').length)
          $('#no_KYC').modal('hide');
        $('.modal-backdrop').remove();
        $('body').removeClass('modal-open');
        $('.navbar-collapse').collapse('hide');

        window.scrollTo(0, 0);

      },200);

    });
    if ($window.localStorage.user) {
      $rootScope.currentUser = JSON.parse($window.localStorage.user);

      $rootScope.globalSettigs = { facebook : 'http://facebook.com', twitter : 'https://twitter.com', instagram : 'https://instagram.com' }
    }
  });
