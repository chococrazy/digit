angular.module('MyApp')
.factory('Project', function($http) {
	return {
		fetchFeatured: function() {
			return $http.get('/project/list/featured');
		},
		fetchForCat : function(tech)
		{
			return $http.get('/project/list/forTech?tech='+tech);
		}
	};
});


angular.module('MyApp')
.factory('Category', function($http) {
	return {
		fetch: function(data) {
			console.log('..')
			return $http.get('/category/list');
		}
	};
});
