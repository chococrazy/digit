angular.module('MyApp')
.controller('HomeCtrl', function($scope,$auth, Project, $location, $routeParams)
{

  $scope.schedule = function(proj)
  {
    $scope.selected_project = proj;
    $('#myModal').modal()

  }

 

	$scope.projects = [];
	$scope.imgIndexes = {};

  $scope.fetchProjects = function(){
		Project.fetchFeatured()
		 .then(function(response)
    {
      $scope.projects = response.data.projects;
    })
    .catch(function(response)
    {
      $scope.projects = [];
      $scope.messages = {
        error: Array.isArray(response.data) ? response.data : [response.data]
      };
    });
	}



  $scope.randomUnitColor  = function(index) {
    if(!$scope.imgIndexes[index] )
      $scope.imgIndexes[index] = $scope.randomUnitColorInner( );
    return $scope.imgIndexes[index];
  }


  $scope.randomUnitColorInner = function() {
    var x = Math.floor(Math.random() * 256);
    var y = Math.floor(Math.random() * 256);
    var z = Math.floor(Math.random() * 256);
    var bgColor = "rgb(" + x + "," + y + "," + z + ")";
    return bgColor;
  }


  $scope.fetchProjectsForCat = function(tech){
    Project.fetchForCat(tech)
     .then(function(response)
    {
      $scope.projects = response.data.projects;
    })
    .catch(function(response)
    {
      $scope.projects = [];
      $scope.messages = {
        error: Array.isArray(response.data) ? response.data : [response.data]
      };
    });
  }

  if($routeParams.tech_name)
  {
    $scope.is_left = true;
    $scope.fetchProjectsForCat($routeParams.tech_name);
  }else
	{
    $scope.is_left = false;
    $scope.fetchProjects();
  }
});



angular.module('MyApp')
.controller('UnitsCtrl', function($scope,$auth, Category)
{
  $scope.isAuthenticated = function() {
    return $auth.isAuthenticated();
  };


   $scope.imgIndexes = {};

  $scope.randomUnitColor  = function(index) {
    if(!$scope.imgIndexes[index] )
      $scope.imgIndexes[index] = $scope.randomUnitColorInner( );
    return $scope.imgIndexes[index];

   }


  $scope.randomUnitColorInner = function() {
    var x = Math.floor(Math.random() * 256);
    var y = Math.floor(Math.random() * 256);
    var z = Math.floor(Math.random() * 256);
    var bgColor = "rgb(" + x + "," + y + "," + z + ")";
    return bgColor;

    }
	$scope.categories = [];
	$scope.fetch = function(){
		Category.fetch()
		 .then(function(response)
    {
      $scope.categories = response.data.categories;
    })
    .catch(function(response)
    {
      $scope.categories = [];
      $scope.messages = {
        error: Array.isArray(response.data) ? response.data : [response.data]
      };
    });
	}

	$scope.fetch();
});
