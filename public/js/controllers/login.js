angular.module('MyApp')
  .controller('LoginCtrl', function($scope, $rootScope, $location, $window, $auth, $routeParams
) {
  // alert($routeParams.successVerify);

  if($routeParams.successVerify == 'successVerify')
  {

    $scope.messages = {
      success: [{msg: 'Account Approved successfully'}]
    };

  }
    $scope.login = function() {


      $auth.login($scope.user)
        .then(function(response) {
          $rootScope.currentUser = response.data.user;
          $window.localStorage.user = JSON.stringify(response.data.user);
            window.location.href = window.location.origin + "/admin_panel"
        })
        .catch(function(response) {
          $scope.messages = {
            error: Array.isArray(response.data) ? response.data : [response.data]
          };
        });
    };

    $scope.authenticate = function(provider) {
      $auth.authenticate(provider)
        .then(function(response) {
          $rootScope.currentUser = response.data.user;
          $window.localStorage.user = JSON.stringify(response.data.user);
          $location.path('/');
        })
        .catch(function(response) {
          if (response.error) {
            $scope.messages = {
              error: [{ msg: response.error }]
            };
          } else if (response.data) {
            $scope.messages = {
              error: [response.data]
            };
          }
        });
    };
  });
