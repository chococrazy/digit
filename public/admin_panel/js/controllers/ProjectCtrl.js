angular.module('MyApp')
  .controller('ProjectCtrl', function($scope, $location, $window, $auth, Project, SweetAlert) {
    $scope.projects = [ ];
    $scope.show = false;

    $scope.list = function(){
		$scope.show = false;
      Project.list()
      .then(function(response) {
        $scope.show = true;
          $scope.projects = response.data.projects;
          setTimeout(function(){
            $('#datatables').DataTable({
              "pagingType": "full_numbers","order": [[ 0, 'desc' ]],
              responsive: true,
          });
          }, 300);
      })
      .catch(function(response) {
          $scope.projects = [ ];
          $scope.show = true;
        $scope.messages = {
          error: Array.isArray(response.data) ? response.data : [response.data]
        };
      });
    }
    $scope.list();


    $scope.requestToDelete = function(id)
    {

        SweetAlert.swal({
          title: "Are you sure?",
          text: "Are you sure you want to delete this?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes, delete it!",
          closeOnConfirm: false
        },  function(e){
          if(!e) return;

        Project.delete({id:id})
        .then(function(response) {
          $scope.messages = {
            success: [response.data]
          };
          $scope.list();
        })
        .catch(function(response) {
          $scope.messages = {
            error: Array.isArray(response.data) ? response.data : [response.data]
          };
        });

      });
    }


  });


angular.module('MyApp')
  .controller('ProjectAddCtrl', function($scope, $location, $window, $auth, Project, Category, $http) {

		$scope.is_new = true;



    $scope.loadTags = function(searchTerm) {
      return $http.get('/api/project/filterCat?search=' + searchTerm);
    };

    $scope.categories = [ ];

    $scope.listCat = function(){
      Category.list()
      .then(function(response) {
        // console.log(response)
          $scope.categories = response.data.categories;
      })
      .catch(function(response) {
          $scope.categories = [ ];
        $scope.messages = {
          error: Array.isArray(response.data) ? response.data : [response.data]
        };
      });
    }
    $scope.listCat();



    $scope.project = {title: '', is_featured : 'No'}
    $scope.title = 'New project';
    $scope.submitForm = function(){
      if($scope.project.title == ''  )
        return;
      Project.add($scope.project)
      .then(function(response) {
        $scope.messages = {
          success: [response.data]
        };
        $location.path('/projects');
      })
      .catch(function(response) {
        $scope.messages = {
          error: Array.isArray(response.data) ? response.data : [response.data]
        };
      });
    }
  });




angular.module('MyApp')
  .controller('ProjectEditCtrl', function($scope, $location,$routeParams, $window, $auth, Project, Category, $http) {
    $scope.project = {title: '', id:$routeParams.id, category_id:'', is_featured:'No' }
    $scope.title = 'Edit project';
		$scope.is_new = false;


        $scope.loadTags = function(searchTerm) {
          return $http.get('/api/project/filterCat?search=' + searchTerm);
        };
    $scope.categories = [ ];

    $scope.listCat = function(){
      Category.list()
      .then(function(response) {
        // console.log(response)
          $scope.categories = response.data.categories;

      })
      .catch(function(response) {
          $scope.categories = [ ];
        $scope.messages = {
          error: Array.isArray(response.data) ? response.data : [response.data]
        };
      });
    }



    $scope.submitForm = function(){
      if($scope.project.title == ''  )
        return;
      Project.update($scope.project)
      .then(function(response) {
        $scope.messages = {
          success: [response.data]
        };
        // $location.path('/faq');
      })
      .catch(function(response) {
        $scope.messages = {
          error: Array.isArray(response.data) ? response.data : [response.data]
        };
      });
    }

    $scope.fetchSingle = function(){
      Project.listSingle($routeParams.id)
      .then(function(response) {
          $scope.project = response.data.project;
          var t = $scope.project.categories.split(',');
          var m = [];
          for (var i = 0; i < t.length; i++) {
            m.push({
              text: t[i].replace('{', '').replace('}', '')
            });
          }
          $scope.project.categories = m;

					$scope.listCat();
      })
      .catch(function(response) {
        $scope.project = {title: '', id:$routeParams.id , category_id:'', is_featured:'No'};
        $scope.messages = {
          error: Array.isArray(response.data) ? response.data : [response.data]
        };
      });
    }

		$scope.fetchSingle();














    $scope.uploaded_images = [];
    $scope.uploadedFile2 = function(element){
      $scope.sec_file_upload_started   = true;
      $scope.sec_file_upload_completed = false;
      $scope.sec_file_upload_failed    = false;
      $scope.$apply(function($scope)
      {
        $scope.sec_files = element.files;
      });
      var fd = new FormData();
      var url = '/media/file';

      fd.append('file', $scope.sec_files[0]);

      $http.post(url, fd,
        {
          withCredentials: false,
          headers:
          {
            'Content-Type': undefined
          },
          transformRequest: angular.identity
        })
        .success(function(data)
        {
					$scope.sec_file_upload_started   = false;
					$scope.sec_file_upload_completed = true;
          $scope.uploaded_images.push({src : data.file_name});

        })
        .error(function(data)
        {
					$scope.sec_file_upload_started = false;
					$scope.sec_file_upload_failed  = true;
          $scope.messages = {
            error: [data]
          };
        });
    }

		$scope.uploadedFile = function(element)
    {

      $scope.file_upload_booking_id = jQuery(element).attr('data-id_key');
      $scope.file_upload_started = true;
      $scope.$apply(function($scope)
      {
        $scope.files = element.files;
      });
      $scope.addFile();
    }
		$scope.file_upload_failed = false;
		$scope.file_upload_completed = false;
    $scope.addFile = function()
    {
      $scope.uploadfile($scope.files);
    }

		$scope.uploadfile = function(files)
    {
      var fd = new FormData();
      var url = '/project/edit/file';
      angular.forEach(files, function(file)
      {
        fd.append('file', file);
      });
      var data = {id : $scope.file_upload_booking_id};
      fd.append("data", JSON.stringify(data));
      $http.post(url, fd,
        {
          withCredentials: false,
          headers:
          {
            'Content-Type': undefined
          },
          transformRequest: angular.identity
        })
        .success(function(data)
        {
					$scope.file_upload_started = false;
           $scope.blogpost =  data.post;
					 $scope.file_upload_completed = true;
          $scope.messages = {
            success: [data]
          };

        })
        .error(function(data)
        {
					$scope.file_upload_started = false;
					$scope.file_upload_failed = true;
          $scope.messages = {
            error: [data]
          };
        });
    };






  });
