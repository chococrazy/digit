angular.module('MyApp', ['ngRoute', 'satellizer','oitozero.ngSweetAlert', 'ngTagsInput'])
  .config(function($routeProvider, $locationProvider, $authProvider)
  {
    $locationProvider.html5Mode(true);
    $routeProvider
      .when('/',
      {
        resolve: { loginRequired: loginRequired },
        templateUrl: 'partials/home.html'
      })
			.when('/category/',
      {
        resolve: { loginRequired: loginRequired },
        controller: 'CategoryCtrl',
        templateUrl: 'partials/category/list.html'
      })
      .when('/category/add',
      {
        resolve: { loginRequired: loginRequired },
        controller: 'CategoryAddCtrl',
        templateUrl: 'partials/category/add.html'
      })
      .when('/category/edit/:id',
      {
        resolve: { loginRequired: loginRequired },
        controller: 'CategoryEditCtrl',
        templateUrl: 'partials/category/add.html',
      })
			.when('/projects/',
      {
        resolve: { loginRequired: loginRequired },
        controller: 'ProjectCtrl',
        templateUrl: 'partials/projects/list.html'
      })
      .when('/projects/add',
      {
        resolve: { loginRequired: loginRequired },
        controller: 'ProjectAddCtrl',
        templateUrl: 'partials/projects/add.html'
      })

      .when('/projects/edit/:id',
      {
        resolve: { loginRequired: loginRequired },
        controller: 'ProjectEditCtrl',
        templateUrl: 'partials/projects/add.html',
      })
    .otherwise(
    {
      templateUrl: 'partials/404.html'
    });

    $authProvider.loginUrl = '/login';
    $authProvider.signupUrl = '/signup';

    function skipIfAuthenticated($location, $auth)
    {
      if ($auth.isAuthenticated())
      {
        $location.path('/');
      }
    }
    function loginRequired($location, $auth, $http, $window)
    {
      if (!$auth.isAuthenticated())
      {
        window.location.href = window.location.origin + "/login"
      }
      $http.get('/me').then(function(res){
        if(res.data && res.data.user)
        {
          if(res.data.user.status  == false)
          {
            $auth.logout();
            delete $window.localStorage.user;
            window.location.href =  window.location.origin ;
          }
          localStorage.setItem('user', JSON.stringify(res.data.user));
        }else{
          window.location.href = window.location.origin + "/login"
        }
      })
      .catch(function(er){
        window.location.href = window.location.origin + "/login"
      })
    }
  })
  .run(function($rootScope, $window, $location)
  {
    if ($window.localStorage.user)
    {
      $rootScope.currentUser = JSON.parse($window.localStorage.user);
    }
    $rootScope.urlhas = function(f)
    {
      return (($location['$$path']).indexOf(f) >= 0);
    }
  });
