
angular.module('MyApp')
  .factory('Category', function($http)
  {
    return {
      list: function()
      {
        return $http.get('/category/list');
      },
      listSingle: function(id)
      {
        return $http.get('/category/single/' + id);
      },
      add: function(data)
      {
        return $http.post('/category/add', data);
      },
      update: function(data)
      {
        return $http.post('/category/edit', data);
      },
      delete: function(data)
      {
        return $http.post('/category/delete', data);
      }
    };
  });


angular.module('MyApp')
  .factory('Project', function($http)
  {
    return {
      list: function()
      {
        return $http.get('/project/list');
      },
      listSingle: function(id)
      {
        return $http.get('/project/single/' + id);
      },
      add: function(data)
      {
        return $http.post('/project/add', data);
      },
      update: function(data)
      {
        return $http.post('/project/edit', data);
      },
      delete: function(data)
      {
        return $http.post('/project/delete', data);
      }
    };
  });

	 
