var bookshelf = require('../config/bookshelf');
var Category = require('./Category');
var Project = bookshelf.Model.extend({
  tableName: 'project',
  hasTimestamps: false,
	category: function() {
    return this.belongsTo(Category);
  }


});

module.exports = Project;
