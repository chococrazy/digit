exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTable('project', function(table) {
      table.increments();
      table.string('title');
			table.string('is_featured');
      table.string('image');
      table.text('categories');
      table.text('details');
      table.text('benifits');
      table.text('innovations');
      table.integer('link_1');
      table.integer('link_1_txt');
      table.integer('link_2');
      table.integer('link_2_txt');
      table.integer('link_3');
			table.integer('link_3_txt');

    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTable('project')
  ])
};
