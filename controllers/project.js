var fs = require('fs');
var Project = require('../models/Project');
var Category = require('../models/Category');


exports.getTags = function(req, res, next)
{
  var s = req.query.search ? req.query.search : "";
  var a = new Category().where('title', 'LIKE',  '%'+s+ '%' ).fetchAll()
  .then(function(resp) {
    var a = resp.pluck('title');
    res.status(200).send(a);
  }).
  catch(function(err){
    console.log(err)
    res.status(200).send([]);
  })

}


exports.deleteProject = function(req, res, next) {
  new Project({ id: req.body.id }).destroy().then(function(user) {
    res.send({ msg: 'The Project has been successfully deleted.' });
  }).catch(function(err) {
    return res.status(400).send({ msg : 'Something went wrong while deleting the Solution' });
  });
};


exports.updateProjectImage = function(req, res, next)
{
  var data  = req.body.data;
  data = JSON.parse(data);
  if(req.file && req.file.path)
  {
    // console.log(req.file)
    var path = req.file.filename;
    var name = req.file.originalname;
    name = name.split('.');
    name = name[name.length-1];
    fs.renameSync('uploads/images/'+path, 'uploads/images/key_' + path + "."+name);

    var booking = new Project().where(
    {
      id: data.id
    }).fetch().then(function(booking){
			if(!booking)
			{
				return res.status(400).send(
					{
						msg : 'Something went wrong while uploading File'
					});;
			}
			booking.save(
			{
				image : "key_" + path + "."+name,
			})
			.then(function(settings)
			{
				res.send(
				{
					post : settings,
					msg     : 'Image uploaded successfully.'
				});
			}).catch(function(err)
			{
				console.log(err)
				res.status(400).send(
				{
					msg : 'Something went wrong while uploading File'
				});
			});
		}).catch(function(err)
			{
				console.log(err)
				res.status(400).send(
				{
					msg : 'Something went wrong while uploading File'
				});
			});
	}


}




exports.updateProject = function(req, res, next) {
  req.assert('title'   , 'Title cannot be blank').notEmpty();

  var errors = req.validationErrors();
  if (errors) {
    return res.status(400).send(errors);
  }


  var categories = [];
  if(req.body.categories)
  {
    for(var i = 0 ; i < req.body.categories.length ; i++)
    {
      categories.push("{"+req.body.categories[i].text+"}");
      new Category().save({title : req.body.categories[i].text}).then(function(){}).catch(function(){});
    }
  }


  var project = new Project({ id: req.body.id });
	var obj = {
		title    : req.body.title,
		is_featured : req.body.is_featured,
		categories : categories.join(','),
    details : req.body.details,
    innovations : req.body.innovations,
    benifits : req.body.benifits,

	};

  if(req.body.remove_media)
  {
    obj.image = '';
  }

  project.save(obj);

  project.fetch().then(function(project) {
    res.send({ project : project, msg: 'Solution has been updated.' });
  }).catch(function(err) {
    res.status(400).send({ msg : 'Something went wrong while updating the Solution' });
  });
};

exports.addProject = function(req, res, next) {
  req.assert('title'   , 'Title cannot be blank').notEmpty();

  var errors = req.validationErrors();

  if (errors) {
    return res.status(400).send(errors);
  }


    var categories = [];
    if(req.body.categories)
    {
      for(var i = 0 ; i < req.body.categories.length ; i++)
      {
        categories.push("{"+req.body.categories[i].text+"}");
        new Category().save({title : req.body.categories[i].text}).then(function(){}).catch(function(err){console.log(err)});
      }
    }



  new Project({
    title    : req.body.title,
		is_featured : req.body.is_featured,
    categories: categories.join(','),
    details : req.body.details,
    innovations : req.body.innovations,
    benifits : req.body.benifits,
  }).save()
  .then(function(project) {
      res.send({ ok:true , msg: 'New Solution has been created successfully.' });
  })
  .catch(function(err) {
    console.log(err)
       return res.status(400).send({ msg: 'Something went wrong while created a new Solution' });
   });
};


exports.listProject = function(req, res, next) {


 new Project( )
 .orderBy('id', 'DESC')
  .fetchAll()
  .then(function(project) {
    if (!project) {
      return res.status(200).send([]);
    }
    return res.status(200).send({ok:true , projects: project.toJSON()});
  })
  .catch(function(err) {
    return res.status(200).send([]);
  });
};



exports.listSingleProject = function(req, res, next)
{
  new Project( ).where('id', req.params.id)
   .fetch()
   .then(function(project) {
     if (!project) {
       return res.status(200).send({id : req.params.id, title: '' });
     }
     return res.status(200).send({ok:true , project: project.toJSON()});
   })
   .catch(function(err) {
     return res.status(400).send({id : req.params.id, title: '', msg: 'failed to fetch from db'});
   })
}

exports.forTech = function(req, res, next) {
  var tech = req.query.tech;
  new Project( )
  .where('categories','LIKE', '%{'+tech+'}%')
  .orderBy('id', 'DESC')
  .fetchAll()
  .then(function(project) {
    if (!project) {
      return res.status(200).send([]);
    }
    return res.status(200).send({ok:true , projects: project.toJSON()});
  })
  .catch(function(err) {
    return res.status(200).send([]);
  });

}


exports.listProjectFeatured = function(req, res, next) {
  new Project( )
  .where('is_featured', 'Yes')
  .orderBy('id', 'DESC')
  .fetchAll()
  .then(function(project) {
    if (!project) {
      return res.status(200).send([]);
    }
    return res.status(200).send({ok:true , projects: project.toJSON()});
  })
  .catch(function(err) {
    return res.status(200).send([]);
  });
};
