var fs = require('fs');

exports.file = function(req, res, next)
{
  // var data = req.body.data;
  // data     = JSON.parse(data);
  if(req.file && req.file.path)
  {
    // console.log(req.file)
    var path = req.file.filename;
    var name = req.file.originalname;
    name = name.split('.');
    name = name[name.length-1];
    fs.renameSync('uploads/images/'+path, 'uploads/images/key_' + path + "."+name);
    var file_path = "key_" + path + "."+name;

    return res.status(200).send({
      ok        : true,
      file_name : file_path,
      file_url  : 'https://' + req.headers.host +'/downloads/images/'+file_path
    });
  }

  return res.status(400).send({
    ok  : false,
    msg : 'Something went wrong while uploading media'
  });



}
