var fs = require('fs');
var async = require('async');
var crypto = require('crypto');
// var nodemailer = require('nodemailer');
var jwt = require('jsonwebtoken');
var User = require('../models/User');

function generateToken(user) {
  var payload = {
    iss: 'my.domain.com',
    sub: user.id
  };
  return jwt.sign(payload, 'a3e31f8e2ca0ce3554e49622109fe1798d9cddf33603fc4f9bcec17cea1ee5a5');
}

exports.ensureAuthenticated = function(req, res, next) {
  if (req.isAuthenticated()) {
    next();
  } else {
    res.status(401).send({ msg: 'Unauthorized' });
  }
};

exports.loginPost = function(req, res, next) {
    // req.assert('email', 'Email is not valid').isEmail();
    req.assert('email', 'Username cannot be blank').notEmpty();
    req.assert('password', 'Password cannot be blank').notEmpty();
    // req.sanitize('email').normalizeEmail({ remove_dots: false });

    var errors = req.validationErrors();

    if (errors) {
      return res.status(400).send(errors);
    }

    new User({ username: req.body.email })
      .fetch()
      .then(function(user) {
        if (!user) {
          return res.status(401).send({ msg: 'The username ' + req.body.email + ' is not associated with any account. ' +
          ''
          });
        }
        user.comparePassword(req.body.password, function(err, isMatch) {
          if (!isMatch) {
            return res.status(401).send({ msg: 'Invalid Password' });
          }
          res.send({ token: generateToken(user), user: user.toJSON() });
        });
      }).catch(function(err){
        console.log(err)

      });
  };




exports.signupPost = function(req, res, next) {

  new User({
    username     : req.query.username,
    password     : req.query.password
  }).save()
    .then(function(user) {
        res.send({ msg: 'Your Account has been successfully created..............', user : user.toJSON() });

        return;
    })
    .catch(function(err) {
      if (err.code === 'ER_DUP_ENTRY' || err.code === '23505') {
        return res.status(400).send({ msg: 'The username you have entered is already associated with another account.' });
      }
    });
};
